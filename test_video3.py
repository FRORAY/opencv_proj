# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
from collections import deque
import numpy as np
import time
import cv2
import imutils
import argparse

# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))
camera.vflip = True
 
# allow the camera to warmup
time.sleep(1)

# rgb color space 
greenLower = (0,0,0)
greenUpper = (100,100,100)


#initialize the list of track points, frame counter, and coordinate deltas
pts = deque(maxlen = 32)
counter = 0
(dX,dY) = (0,0)
direction = ""
print(pts)

# capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        # grab the raw NumPy array representing the image, then initialize the timestamp
        # and occupied/unoccupied text
        image = frame.array

        #resize frame, convert to HSV color space
        frame = imutils.resize(image, width=500)
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # mask for all colors, dilate and erode to remove
        # small blobs left in the mask
        mask = cv2.inRange(hsv, greenLower, greenUpper)
        mask = cv2.erode(mask, None, iterations=3)
        mask = cv2.dilate(mask, None, iterations=3)

        # contours in mask
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        center = None


        # only proceed if at least one contour was found
        if len(cnts) > 0:
                c = max(cnts, key=cv2.contourArea)
                ((x, y), radius) = cv2.minEnclosingCircle(c)
                M = cv2.moments(c)
                center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                cv2.circle(frame, center, 7, (255,255,255), -1)
                pts.appendleft(center)
        for i in np.arange(1, len(pts)) :
                if pts[i-1] is None or pts [i] is None:
                        continue
                if counter >= 10 and i ==1 and pts[-10] is not None:
                        #print(pts[-10])
                        # compute the difference between the x and y
                        # coordinates and re-initialize the direction
                        # text variables
                        dX = pts[-10][0] - pts[i][0]
                        dY = pts[-10][1] - pts[i][1]
                        (dirX, dirY) = ("", "")

                        # ensure there is significant movement in the
                        # x-direction
                        if np.abs(dX) > 20:
                                dirX = "East" if np.sign(dX) == 1 else "West"

                        # ensure there is significant movement in the
                        # y-direction
                        if np.abs(dY) > 20:
                                dirY = "North" if np.sign(dY) == 1 else "South"

                        # handle when both directions are non-empty
                        if dirX != "" and dirY != "":
                                direction = "{}-{}".format(dirY, dirX)

                        # otherwise, only one direction is non-empty
                        else:
                                direction = dirX if dirX != "" else dirY
                #otherwise, compute the thickness of the line
                #draw the connecting lines
                thickness = int(np.sqrt(32 / float(i + 1)) + 2.5)
                cv2.line(frame, pts[i -1], pts[i], (0,0,255),thickness)
        #show the movement deltas and the direction of movement on the frame
        cv2.putText(frame, direction, (10,30), cv2.FONT_HERSHEY_SIMPLEX,0.35,
                    (255,0,0), 1)
        cv2.putText(frame, "dx: {}, dy: {}".format(dX,dY), (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                    0.35, (255,0,0), 1)
        #show frame
        cv2.imshow("Frame", frame)
        cv2.imshow("Mask", mask)

        key = cv2.waitKey(1) & 0xFF
        counter += 1

        # clear the stream in preparation for the next frame
        rawCapture.truncate(0)

        # if the `f` key was pressed, break from the loop
        if key == ord('f'):
                break

cv2.destroyAllWindows()
